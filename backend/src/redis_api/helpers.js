const toObject = (input_keys, input_values) =>
  input_keys.reduce((o, k, i) => {
    o[k] = input_values[i];
    return o;
  }, {});

function addKeysToRedisResponse(keys, redis_response) {
  keys = typeof(keys) === 'object' ? keys : [keys]
  return [].map.call(redis_response, item => {
    return toObject(keys, item);
  });
}

function buildApiResponse(success, data) {
  return {
    success,
    data
  };
}

module.exports = {
  addKeysToRedisResponse,
  buildApiResponse
};
