// body for express-validation
const { body, validationResult, query } = require('express-validator');

const redis = require('redis');
const bluebird = require('bluebird');
// this adds 'Async' to all redis function (i. e. get --> getAsync)
// and allows for promissification of all redis-calls
// thus, you can wait for values to be returned and assign
// them to variables
bluebird.promisifyAll(redis);

const REDIS_PORT = process.env.PORT || 6379;
const REDIS_SERVER = process.env.REDIS_SERVER || '';

const client = redis.createClient((port = REDIS_PORT), (host = REDIS_SERVER));
const multi = client.multi();

const helpers = require('./helpers');

async function addMood(req, res, next) {
  console.log(`${req.method}: ${req.url}`);

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(200).json({ errors: errors.array() });
    return;
  }

  const { user_id, mood, location, activities } = req.body;

  const mood_id = await client.incrAsync('next_mood_id');
  const timestamp = new Date().getTime();

  multi.hmset([
    `mood:${mood_id}`,
    'user_id',
    user_id,
    'mood',
    mood,
    'mood_id',
    mood_id,
    'timestamp',
    timestamp,
    'activities',
    activities,
    'location',
    location
  ]);
  multi.lpush(`timeline:${user_id}`, mood_id);

  multi.exec((err, resp) => {
    if (err) {
      console.error(err);
      res.sendStatus(500);
    } else {
      console.log(`mood:${mood_id} set`);
      res.status(200).send({
        mood_id,
        mood,
        location,
        activities,
        timestamp
      });
    }
  });
}

async function getMoods(req, res, next) {
  console.log(`${req.method}: ${req.url}`);

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(200).json({ errors: errors.array() });
    return;
  }

  const { user_id, from_index, to_index } = req.query;
  const mood_keys = [
    'user_id',
    'mood',
    'timestamp',
    'activities',
    'location',
    'mood_id'
  ];

  const mood_ids = await client.lrangeAsync(
    `timeline:${user_id}`,
    from_index,
    to_index
  );
  console.log(`${mood_ids}`);
  const max_moods = await client.llenAsync(`timeline:${user_id}`);

  mood_ids.forEach(mood_id => {
    multi.hmget(`mood:${mood_id}`, mood_keys);
  });

  multi.exec(async (err, resp) => {
    if (err) {
      console.error(err);
      res.sendStatus(500);
    } else {
      const res_with_keys = await helpers.addKeysToRedisResponse(
        mood_keys,
        resp
      );
      const api_response = helpers.buildApiResponse(true, res_with_keys);
      api_response['max_mood'] = max_moods - 1;
      object_response = res.status(200).send(api_response);
    }
  });
}

async function editMood(req, res) {
  console.log(`${req.method}: ${req.url}`);

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(200).json({ errors: errors.array() });
    return;
  }

  const { mood_id, mood, location, activities } = req.body;
  const timestamp = new Date().getTime();
  client.hmset(
    [
      `mood:${mood_id}`,
      'mood',
      mood,
      'timestamp',
      timestamp,
      'activities',
      activities,
      'location',
      location
    ],
    res.sendStatus(200)
  );
}

async function deleteMood(req, res) {
  console.log(`${req.method}: ${req.url}`);

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(200).json({ errors: errors.array() });
    return;
  }

  const { mood_id, user_id } = req.body;
  client.lrem([`timeline:${user_id}`, -1, mood_id], res.sendStatus(200));
}

function validate(method) {
  switch (method) {
    case 'addMood': {
      return [
        body('user_id').isInt(),
        body('mood').isString(),
        body('location').isString(),
        body('activities').isArray()
      ];
    }
    case 'getMoods': {
      return [
        query('user_id').isInt(),
        query('from_index').isInt(),
        query('to_index').isInt()
      ];
    }
    case 'deleteMood': {
      return [
        body('user_id').isInt(),
        body('mood_id').isInt()
      ];
    }
  }
}

module.exports = {
  addMood,
  getMoods,
  editMood,
  deleteMood,
  validate
};
