const { body, validationResult, query } = require('express-validator');
const helpers = require('./helpers');
const redis = require("redis");
const bluebird = require("bluebird");
// this adds 'Async' to all redis function (i. e. get --> getAsync)
// and allows for promissification of all redis-calls
// thus, you can wait for values to be returned and assign
// them to variables
bluebird.promisifyAll(redis);

const REDIS_PORT = process.env.PORT || 6379;
const REDIS_SERVER = process.env.REDIS_SERVER || "";

const client = redis.createClient((port = REDIS_PORT), (host = REDIS_SERVER));
const multi = client.multi();



async function getTopActivities(req, res) {
    console.log(`${req.method}: ${req.url}`);

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      res.status(200).json({errors: errors.array()});
      return
    }

    const { user_id } = req.query;
    const activities = await client.zrangeAsync(`activities:${user_id}`, -10, -1)
    const res_with_keys = {'activities': activities};
    const api_response = helpers.buildApiResponse(true, res_with_keys);
    object_response = res.status(200).send(api_response);
    res.sendStatus
}

async function addActivityType(req, res) {
  /**
   * adds an activity to the user's list of activitites
   */
    console.log(`${req.method}: ${req.url}`);
    
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      res.status(200).json({errors: errors.array()});
      return
    }

    const { user_id, activities } = req.body;
    activities.forEach(activity => {
        // multi.sadd(`activities:${user_id}`, activity);
        multi.zincrby(`activities:${user_id}`, 1, activity)
    })

    multi.exec(async (err, resp) => {
        if (err) {
          console.error(err);
          res.sendStatus(500, err);
        } else {
          res.sendStatus(200, resp)
        }
      });
};

function validate(method) {
  switch (method) {
    case 'getActivityTypes': {
      return [
        query('user_id').isInt()
      ]
    }
    case 'addActivityType': {
      return [
        body('user_id').isInt(),
        body('activities').isArray(),
      ]
    }
  }
}

module.exports = {
    addActivityType,
    getTopActivities,
    validate
};