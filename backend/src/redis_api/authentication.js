const redis = require('redis');
const bluebird = require('bluebird');
// this adds 'Async' to all redis function (i. e. get --> getAsync)
// and allows for promissification of all redis-calls
// thus, you can wait for values to be returned and assign
// them to variables
bluebird.promisifyAll(redis);

const randtoken = require('rand-token');
// body for express-validation
const { body, validationResult } = require('express-validator');

const REDIS_PORT = process.env.PORT || 6379;
const REDIS_SERVER = process.env.REDIS_SERVER || '';

const client = redis.createClient((port = REDIS_PORT), (host = REDIS_SERVER));
const multi = client.multi();

async function singUp(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(422).json({ errors: errors.array() });
    return;
  }

  const { username, password } = req.body;

  const userExists = await client.hmgetAsync(`user:${username}`, 'user_id');
  if (Boolean(userExists[0])) {
    res
      .status(200)
      .send({ erros: [{ msg: `user:${username} already taken.` }] });
    return;
  } else {
    const user_id = await client.incrAsync('next_user_id');
    const timestamp = new Date().getTime();

    multi.hmset([
      `user:${username}`,
      'user_id',
      user_id,
      'timestamp', // timestamp of user creation
      timestamp,
      'username',
      username,
      'password', // change to saving a hash of the pw instead of the actual password
      password
    ]);

    const authToken = await createAuthToken(user_id);
    // actually run the requests against redis
    multi.exec((err, resp) => {
      if (err) {
        res.status(200).send({
          errors: [
            {
              msg: err,
              location: 'body'
            }
          ]
        });
      } else {
        res.status(200).send({
          user_id,
          username,
          authToken
        });
      }
    });
  }
}

async function createAuthToken(user_id) {
  // TODO: make authToken random
  const authToken = randtoken.generate(32).toString();
  console.log(`auth:${user_id}`, authToken);
  await client.set(`auth:${user_id}`, authToken); // TODO: add expiration
  return authToken;
}

async function getAuthToken(user_id) {
  const authToken = await client.getAsync(`auth:${user_id}`);
  return authToken;
}

async function logIn(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(200).json({ errors: errors.array() });
    return;
  }

  const { username, password } = req.body;
  const actual_user_data = await client.hmgetAsync(
    `user:${username}`,
    'password',
    'user_id'
  );
  console.log(actual_user_data[0], password);
  if (password === actual_user_data[0]) {
    const authToken = await createAuthToken(actual_user_data[1]);
    console.log(authToken);

    res.status(200).send({
      authToken,
      user_id: actual_user_data[1]
    });
  } else {
    res.status(200).send({
      errors: [
        {
          msg: 'wrong login credentials',
          param: 'username',
          location: 'body'
        }
      ]
    });
  }
}

async function logOut(req, res) {
  const { user_id } = req.body;
  await client.del(`auth:${user_id}`);
  res.send(200);
}

async function ensureLoggedIn(req, res, next) {
  try {
    let user_id, atuhToken;

    if (req.method == 'GET') {
      user_id = req.query.user_id;
      authToken = req.query.authToken;
    } else {
      user_id = req.body.user_id;
      authToken = req.body.authToken;
    }

    const actual_authToken = await getAuthToken(user_id);
    console.log(`auth:${user_id}`, actual_authToken);
    if (actual_authToken === authToken) {
      next();
    } else {
      res.status(200).send({
        errors: [
          {
            msg: 'authentication data invalid',
            param: 'username',
            location: 'body'
          }
        ]
      });
    }
  } catch (err) {
    console.warn(err);
    res.status(200).send('please login');
  }
}

// START TODO: rewrite to match auth endpoint
function validate(method) {
  switch (method) {
    case 'signUp': {
      return [
        body('username')
          .isString()
          .withMessage('please provide a username'),
        body('password').isString()
      ];
    }
    case 'logIn': {
      return [body('username').isString(), body('password').isString()];
    }
  }
}
// END TODO

module.exports = {
  singUp,
  logIn,
  logOut,
  ensureLoggedIn,
  validate
};
