const express = require('express');
const app = express();

// use standard cors options because I dont really understand them 
// enough in order to specify my own :-)
const cors = require('cors');
const corsOptions = {
  origin: 'localhost',
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
  allowHeaders: ['Content-Type', 'Authorization'],
  preflightContinue: true
}
// app.use(cors(corsOptions))
app.use(cors())
// use json in order to be able to work with json-bodies
// in the requests
app.use(express.json());

const redis_auth = require('./src/redis_api/authentication');
const redis_moods = require('./src/redis_api/moods');
const redis_user_settings = require('./src/redis_api/user_settings');
const PORT = process.env.PORT || 5000;

app.post('/moods', redis_auth.ensureLoggedIn, redis_moods.validate('addMood'), redis_moods.addMood);
app.get('/moods', redis_auth.ensureLoggedIn, redis_moods.validate('getMoods'), redis_moods.getMoods);
app.put('/moods', redis_auth.ensureLoggedIn, redis_moods.validate('addMood'), redis_moods.editMood);
app.delete(
  '/moods',
  redis_auth.ensureLoggedIn,
  redis_moods.validate('deleteMood'),
  redis_moods.deleteMood
);

app.post(
  '/user_settings/activities',
  redis_auth.ensureLoggedIn,
  redis_user_settings.validate('addActivityType'),
  redis_user_settings.addActivityType
);
app.get(
  '/user_settings/activities',
  redis_auth.ensureLoggedIn,
  redis_user_settings.validate('getActivityTypes'),
  redis_user_settings.getTopActivities
);

app.post('/auth/signup', redis_auth.validate('signUp'), redis_auth.singUp);
app.post('/auth/login', redis_auth.validate('logIn'), redis_auth.logIn);
app.post('/auth/logout', redis_auth.logOut);

app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
});
