import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/today',
    name: 'today',
    meta: {
      title: 'Mood Overview - Today',
      content: 'The overview of all moods that were saved so far.',
      requiresAuth: true
    },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/Today.vue')
  },
  {
    path: '/add',
    name: 'add',
    meta: {
      title: 'Add Mood - Today',
      content: 'Add a new mood.',
      requiresAuth: true
    },
    component: () => import(/* webpackChunkName: "about" */ '../views/Add.vue')
  },
  {
    path: '/user',
    name: 'user',
    meta: {
      title: 'User Settings - Today',
      content: 'Look, user settings here.',
      requiresAuth: true
    },
    component: () => import(/* webpackChunkName: "about" */ '../views/User.vue')
  },
  {
    path: '/calendar',
    name: 'calendar',
    meta: {
      title: 'Mood Calendar - Today',
      content: 'Monthly Mood Overview.',
      requiresAuth: true
    },
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/Calendar.vue')
  },
  {
    path: '/',
    name: 'Login Sign Up',
    meta: {
      title: 'Join - Today'
    },
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/LoginSignup.vue')
  },
  {
    path: '*',
    name: 'redirect - login Sign up',
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/LoginSignup.vue')
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

const DEFAULT_TITLE = 'Today';
router.beforeEach((to, from, next) => {
  window.scrollTo(0, 0);
  document.title = to.meta.title || DEFAULT_TITLE;

  if (!to.matched.some(record => record.meta.requiresAuth)) {
    next();
  } else {
    if (
      localStorage.getItem('authToken') != null ||
      localStorage.getItem('user_id') != null
    ) {
      next();
    } else {
      next({ path: '/' });
    }
  }
});

export default router;
