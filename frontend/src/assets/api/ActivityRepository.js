import axios from 'axios';
const resource = 'user_settings/activities';
const prod_env = require('../../../prod.env');

const backend_host = prod_env.VUE_APP_URL;

function buildAuthPayload(payload) {
  return {
    ...payload,
    user_id: localStorage.getItem('user_id'),
    authToken: localStorage.getItem('authToken')
  };
}

export default {
  get(payload) {
    // eslint-disable-next-line no-console
    console.log(resource, payload);
    return axios.get(`${backend_host}/${resource}`, {
      params: buildAuthPayload(payload)
    });
  },
  post(payload) {
    return axios.post(`${backend_host}/${resource}`, buildAuthPayload(payload));
  },
  delete(payload) {
    return axios.delete(
      `${backend_host}/${resource}`,
      buildAuthPayload(payload)
    );
  },
  put(payload) {
    return axios.put(`${backend_host}/${resource}`, buildAuthPayload(payload));
  }
};
