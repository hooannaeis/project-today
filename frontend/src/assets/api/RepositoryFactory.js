import MoodRepository from './MoodRepository';
import ActivityRepository from './ActivityRepository';
import GeoRepository from './GeoRepository';
import UserRepository from './UserRepository';

const repositories = {
  moods: MoodRepository,
  activities: ActivityRepository,
  geos: GeoRepository,
  user: UserRepository
};

export const RepositoryFactory = {
  get: name => repositories[name]
};
